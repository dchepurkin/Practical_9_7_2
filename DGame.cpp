﻿#include "DGame.h"

#include <iostream>

#include "DRender.h"
#include "DInput.h"

DGame::~DGame()
{
    delete Graphics;
    delete KeyboardInput;
}

void DGame::Start()
{
    Graphics = new DRender(this);
    KeyboardInput = new DInput(this);
    srand(static_cast<unsigned>(time(nullptr)));

    while(!bEndGame)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(SpawnEnemyRate));
        Tick();
    }
}

void DGame::Tick() const
{
    const auto SpawnPosition = rand() % 2 == 0 ? EPosition::UP : EPosition::DOWN;
    if(Graphics)
    {
        Graphics->SpawnEnemy(SpawnPosition);
    }
}

void DGame::EndGame()
{
    bEndGame = true;
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    system("cls");
    std::cout << "Game Over\n";
    system("pause");
}

void DGame::SetPlayerPosition(EPosition NewPosition)
{
    if(!Graphics || NewPosition == CurrentPosition) return;
    CurrentPosition = NewPosition;
    Graphics->PlayerMove(CurrentPosition);
}
