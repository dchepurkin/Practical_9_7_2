﻿#pragma once

enum class EPosition
{
    UP,
    DOWN
};

enum class EGridInfo
{
    EMPTY,
    BORDER,
    PLAYER,
    ENEMY
};

struct DVector
{
    explicit DVector(const int x = 0, const int y = 0)
        : X(x),
          Y(y) { }

    int GetX() const { return X; }
    int GetY() const { return Y; }

private:
    int X = 0;
    int Y = 0;
};
