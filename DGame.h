﻿#pragma once
#include "DTypes.h"

class DRender;
class DInput;

class DGame
{
public:
    ~DGame();
    void Start();
    void Tick() const;
    void EndGame();
    bool IsGameOver() const { return bEndGame; }

    int GetFPS() const { return FPS; }
    void SetFPS(const int NewFPS) { FPS = NewFPS; }

    void SetPlayerPosition(EPosition NewPosition);

private:
    DRender* Graphics = nullptr;
    DInput* KeyboardInput = nullptr;
    EPosition CurrentPosition = EPosition::DOWN;
    int FPS = 4;
    int SpawnEnemyRate = 1200;
    bool bEndGame = false;

public:
    const DVector ScreenSize{10, 2};
};
