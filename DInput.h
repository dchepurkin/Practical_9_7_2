﻿#pragma once
#include <thread>

class DGame;

class DInput
{
public:
    explicit DInput(DGame* game);
    ~DInput();

private:
    std::thread KeyboardThread;
    DGame* Game = nullptr;

    void InputTick();
};
