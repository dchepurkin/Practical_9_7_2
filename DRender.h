﻿#pragma once
#include <map>
#include <mutex>
#include <thread>
#include <vector>
#include "DTypes.h"

class DGame;

class DRender
{
public:
    explicit DRender(DGame* game);
    ~DRender();
    void PlayerMove(EPosition PlayerPosition);
    void SpawnEnemy(EPosition EnemyPosition);

private:
    std::thread RenderThread;
    std::thread EnemyMoveThread;
    std::mutex Mutex;
    DGame* Game;

    int GridSize = 5;
    std::vector<std::vector<EGridInfo>> Grid;
    std::map<EGridInfo, char> GridPalete{
        {EGridInfo::EMPTY, ' '},
        {EGridInfo::ENEMY, '@'},
        {EGridInfo::BORDER, '-'},
        {EGridInfo::PLAYER, '#'}
    };

    void Tick();
    bool IsBorder(const int InY) const;
    void InitGrid();
    int GetXSize() const;
    int GetYSize() const;
    void EnemyMove();
};
