﻿#include <iostream>

#include "DInput.h"
#include "DGame.h"
#include <Windows.h>

DInput::DInput(DGame* game)
    : Game(game)
{
    KeyboardThread = std::thread(&DInput::InputTick, this);
}

DInput::~DInput()
{
    KeyboardThread.join();
}

void DInput::InputTick()
{
    if(!Game) return;

    while(!Game->IsGameOver())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(Game->GetFPS()));
        if(GetKeyState('W') & 0x8000)
        {
            Game->SetPlayerPosition(EPosition::UP);
        }
        else if(GetKeyState('S') & 0x8000)
        {
            Game->SetPlayerPosition(EPosition::DOWN);
        }
        else if(GetKeyState(VK_ESCAPE) & 0x8000)
        {
            Game->EndGame();
        }
    }
}
