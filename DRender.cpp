﻿#include "DRender.h"
#include "DGame.h"
#include <iostream>

DRender::DRender(DGame* game)
    : Game(game)
{
    InitGrid();
    PlayerMove(EPosition::DOWN);
    RenderThread = std::thread(&DRender::Tick, this);
    EnemyMoveThread = std::thread(&DRender::EnemyMove, this);
}

DRender::~DRender()
{
    RenderThread.join();
    EnemyMoveThread.join();
}

void DRender::PlayerMove(EPosition PlayerPosition)
{
    std::unique_lock<std::mutex> UniqueLock(Mutex);
    for(int i = 1; i < GetYSize() - 1; ++i)
    {
        for(int j = 0; j < GetXSize() / (GridSize * 2); ++j)
        {
            if(PlayerPosition == EPosition::UP)
            {
                if(Grid[i][j] == EGridInfo::ENEMY && i < GridSize)
                {
                    Game->EndGame();
                    return;
                }
                Grid[i][j] = i < GridSize ? EGridInfo::PLAYER : EGridInfo::EMPTY;
            }
            else
            {
                if(Grid[i][j] == EGridInfo::ENEMY && i >= GridSize)
                {
                    Game->EndGame();
                    return;
                }
                Grid[i][j] = i >= GridSize ? EGridInfo::PLAYER : EGridInfo::EMPTY;
            }
        }
    }
}

void DRender::SpawnEnemy(EPosition EnemyPosition)
{
    std::unique_lock<std::mutex> UniqueLock(Mutex);

    const int SpawnPoint = EnemyPosition == EPosition::UP ? 1 : GridSize;
    for(int i = SpawnPoint; i < SpawnPoint + GridSize - 1; ++i)
    {
        Grid[i][GetXSize() - 1] = EGridInfo::ENEMY;
    }
}

void DRender::Tick()
{
    if(!Game) return;

    while(!Game->IsGameOver())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(Game->GetFPS()));
        std::unique_lock<std::mutex> UniqueLock(Mutex);
        system("cls");
        for(int i = 0; i < GetYSize(); ++i)
        {
            for(int j = 0; j < GetXSize(); ++j)
            {
                std::cout << GridPalete[Grid[i][j]];
            }
            std::cout << std::endl;
        }
    }
}

bool DRender::IsBorder(const int InY) const
{
    return InY == 0 || InY == GetYSize() - 1;
}

void DRender::InitGrid()
{
    for(int i = 0; i < GetYSize(); ++i)
    {
        Grid.emplace_back(std::vector<EGridInfo>(GetXSize()));
        for(int j = 0; j < GetXSize(); ++j)
        {
            Grid[i][j] = IsBorder(i) ? EGridInfo::BORDER : EGridInfo::EMPTY;
        }
    }
}

int DRender::GetXSize() const
{
    return Game ? Game->ScreenSize.GetX() * GridSize : 0;
}

int DRender::GetYSize() const
{
    return Game ? Game->ScreenSize.GetY() * GridSize : 0;
}

void DRender::EnemyMove()
{
    while(!Game->IsGameOver())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        std::unique_lock<std::mutex> UniqueLock(Mutex);
        for(int i = 0; i < GetYSize(); ++i)
        {
            for(int j = 0; j < GetXSize(); ++j)
            {
                if(Grid[i][j] == EGridInfo::ENEMY)
                {
                    if(j > 0)
                    {
                        if(Grid[i][j - 1] == EGridInfo::PLAYER)
                        {
                            Game->EndGame();
                            return;
                        }
                        Grid[i][j - 1] = EGridInfo::ENEMY;
                    }

                    Grid[i][j] = EGridInfo::EMPTY;
                }
            }
            std::cout << std::endl;
        }
    }
}
